﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour {
    public static int vint = 1000;
    public float ballVelocity = Mathf.Clamp(vint, 500, 1500);

    public Rigidbody rb;
    bool isPlay;
    int randInt;
    public AudioClip sfx;
    public GameObject vfx;
    public GameObject startUI; 


	void Start () {
		startUI = GameObject.Find("StartPanel");
	}

    void Awake()
    {
        rb = gameObject.GetComponent<Rigidbody>();
        randInt = Random.Range(1, 3);
        
    }
	
	// Update is called once per frame
	void Update () {
		if(Input.GetKeyDown(KeyCode.Space) == true && isPlay == false)
        {
            
            transform.parent = null;
            isPlay = true;
            rb.isKinematic = false;
            startUI.SetActive(false);
            if (randInt == 1)
            {
                rb.AddForce(new Vector3(ballVelocity, ballVelocity, 0));
            }
            if (randInt == 2)
            {
                rb.AddForce(new Vector3(-ballVelocity, -ballVelocity, 0));
            }

    
        }
	}

   public void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.tag == "Wall")
        {
            GetComponent<AudioSource>().Play();
        }

        if (col.gameObject.tag == "Paddle1")
        {
            GetComponent<AudioSource>().Play();
            rb.AddForce(new Vector3(1, 1, 0));
        }

        if (col.gameObject.tag == "Paddle2")
        {
            GetComponent<AudioSource>().Play();
            rb.AddForce(new Vector3(-1, -1, 0));
        }
    }

    
}
