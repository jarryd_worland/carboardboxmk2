﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallBig : MonoBehaviour
{
    public Vector3 bigScale;

    public void Bigger()
    {
        transform.localScale = bigScale;
    }

}
