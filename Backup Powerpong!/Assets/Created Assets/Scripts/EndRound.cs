﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndRound : MonoBehaviour {

    public GameObject Ball;
    public GameObject boundR;
    public GameObject boundL;
    public AudioClip sfx;
    public Vector3 ballStart;
    public Vector3 boundRStart;
    public Vector3 boundLStart;
    public GameObject startUI;

    // Use this for initialization
    void Start () {
        ballStart = new Vector3(113.3f, -81.4f, 1.3f);
        boundRStart = new Vector3(229.43f, -85.5f, 0.3f);
        boundLStart = new Vector3(-3.31f, -85.5f, 0.3f);
        startUI = GameObject.Find("StartPanel");
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter(Collider other)
    {
       if (other.tag == "Ball")
       {
            Instantiate(Ball, ballStart, Quaternion.identity);
            AudioSource.PlayClipAtPoint(sfx, ballStart);
            Destroy(other.gameObject);
            startUI.SetActive(true);

        }
    }
}
