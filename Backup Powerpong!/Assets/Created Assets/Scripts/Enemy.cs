﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour {


    public float paddleSpeed = 1;
    public Vector3 playerPos;

	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

        float yPos = gameObject.transform.position.y + paddleSpeed;
        float yNeg = gameObject.transform.position.y - paddleSpeed;

        if (Input.GetKey(KeyCode.I))
            {
            playerPos = new Vector3(210, Mathf.Clamp(yPos, -124, -42), 0);
            gameObject.transform.position = playerPos;
        }

        if (Input.GetKey(KeyCode.K))
            {
            playerPos = new Vector3(210, Mathf.Clamp(yNeg, -124, -42), 0);
            gameObject.transform.position = playerPos;
        }

        

    }
}
