﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Paddle : MonoBehaviour {


    public float paddleSpeed = 1;
    public Vector3 playerPos;
    public GameObject Player1;
    public GameObject Ball;
    public BallBig Big;
    public Ball BallScript;
    public AudioClip sfx;
    

	void Start () {
        //Player1 = GameObject.Find("Player1UI");
        Ball = GameObject.Find("Ball");
        BallScript = Ball.gameObject.GetComponent<Ball>();
        Big = Ball.GetComponent<BallBig>();
    }
	
	// Update is called once per frame
	void Update () {

        float yPos = gameObject.transform.position.y + paddleSpeed;
        float yNeg = gameObject.transform.position.y - paddleSpeed;

        if (Input.GetKey(KeyCode.W))
            {
            playerPos = new Vector3(20, Mathf.Clamp(yPos, -124, -42), 0);
            gameObject.transform.position = playerPos;
        }

        if (Input.GetKey(KeyCode.S))
            {
            playerPos = new Vector3(20, Mathf.Clamp(yNeg, -124, -42), 0);
            gameObject.transform.position = playerPos;
        }

        if (Input.GetKeyDown(KeyCode.A) && Player1.gameObject.activeSelf)
        {
            Big.Bigger();
            BallScript.ballVelocity = BallScript.ballVelocity + 1000;
            Player1.SetActive(false);
        }

        if (Input.GetKeyDown(KeyCode.D) && Player1.gameObject.activeSelf)
        {
            //Instantiate(Ball, Ball.transform.position);
            //BallScript.ballVelocity = BallScript.ballVelocity + 1000;
            Player1.SetActive(false);
        }

    }

    void OnColliderEnter(BoxCollider col)
    {
        AudioSource.PlayClipAtPoint(sfx, playerPos);
        
    }

    private void OnTriggerEnter(Collider col)
    {
        Player1.SetActive(true);
        BallScript.ballVelocity = (BallScript.ballVelocity / 0.95f);
        BallScript.rb.velocity = new Vector3(-2, -2, 0);

    }
}
